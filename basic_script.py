import geopandas as gp
import pandas as pd, numpy as np, matplotlib.pyplot as plt
import multiprocessing as mp
from sklearn.cluster import DBSCAN
from geopy.distance import great_circle
from shapely.geometry import MultiPoint
from math import sin, cos, sqrt, atan2,radians
from multiprocessing import Pool
from multiprocessing import shared_memory
from data_models import FireDataStorage, CitiesDataStorage

def get_centermost_point(cluster):
    centroid = (MultiPoint(cluster).centroid.x, MultiPoint(cluster).centroid.y)
    centermost_point = min(cluster, key=lambda point: great_circle(point, centroid).m)
    return tuple(centermost_point)

def filter(row, lat2, lon2, max):
    return getDist(row.centroid.y, row.centroid.x,lat2,lon2) < max

def getDist(lat1, lon1, lat2, lon2):
    """
    Distance function between two lat/lon
    """
    R = 6373.0

    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R * c

def calc_fire_clusters(fire_hotspots, fire_cluster_distance_km, fire_group_minimum_size):     
    """
    Group fires list to clusters by distance and minimun group size
    """
    
    fire_clusters = []
    for name, group in fire_hotspots:
        # cluster within groups
        # see more: https://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
        coords = group[['latitude', 'longitude']].to_numpy()
        kms_per_radian = 6371.0088
        epsilon = fire_cluster_distance_km / kms_per_radian
        db = DBSCAN(eps=epsilon, min_samples=1, algorithm='ball_tree', metric='haversine').fit(np.radians(coords))
        cluster_labels = db.labels_
        num_clusters = len(set(cluster_labels))
        data = [coords[cluster_labels == n] for n in range(num_clusters)]
        data = [x for x in data if len(x) > fire_group_minimum_size]
        print('len(data)', len(data))

        clusters = pd.Series(data)
        num_clusters = len(clusters)

        print('Number of clusters: {}'.format(num_clusters))
        centermost_points = clusters.map(get_centermost_point)
        lats, lons = zip(*centermost_points)
        rep_points = gp.GeoDataFrame({'longitude': lons, 'latitude': lats})
        fire_clusters.append(rep_points)
        break
    
    return fire_clusters 

def count_city_nearby_fires(city, fire_clusters, city_distance_km):
    """
    Gets the total number of fires within a given distance from a city
    """
    result = 0
    for cluster in fire_clusters:
        for index, row in cluster.iterrows():
            result += filter(city, row['latitude'], row['longitude'], city_distance_km)
    
    return city.NAME, result


class FirePerCityCounter:
    def __init__(self, fire_storage, cities_storage):
        print("FirePerCityCounter Init")
        self.fire_storage   = fire_storage
        self.cities_storage = cities_storage 

    def count_nearby_fires(self, fire_cluster_distance_km, city_distance_km, fire_group_minimum_size):
        """
        Outputs a csv file that tells how many times each city was within x distance of a city
        """
        # Get fires and cities data 
        fires_hotspots_df = self.fire_storage
        cities = self.cities_storage.get_cities_data() 

        # Group by 5 days using pd.Grouper
        fire_hotspots_by_day = fires_hotspots_df.groupby([pd.Grouper(key='acq_date', freq='5D')])
        fire_clusters = calc_fire_clusters(fire_hotspots_by_day, fire_cluster_distance_km, fire_group_minimum_size)
        
        # Parallel analyze - For each city, count to how many fires were in the given range
        results = []
        pool = Pool(mp.cpu_count() - 1)
        for index, city in cities.iterrows():
            params = [city, fire_clusters, city_distance_km]
            results.append(pool.apply_async(count_city_nearby_fires, params))
        
        output = {}

        for r in results:
            result = r.get()
            output[result[0]] = result[1]
            print(f"{result[0]} = {result[1]}") # TODO - Log it to a debug file per debug configuration

        pool.close()
        pool.join()

        return (output)

if __name__ == '__main__':
    fires_per_city_counter = FiresPerCityCounterManager()
    fires_per_city_counter.count_fires_per_city(fire_cluster_distance_km=10, city_distance_km=100, fire_group_minimum_size=10)
    # count_nearby_fires2(fire_cluster_distance_km=10, city_distance_km=100, fire_group_minimum_size=10)
