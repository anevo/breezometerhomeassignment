import os
import uvicorn
import numpy as np
import pandas as pd
from fastapi import FastAPI
from pydantic import BaseModel
from data_models import FireDataStorage, CitiesDataStorage
from basic_script import FirePerCityCounter
from fires_service import FiresPerCityCounterManager

app = FastAPI()

class FiresCounterBodyRequest(BaseModel):
    fire_cluster_distance_km : int 
    city_distance_km : int 
    fire_group_minimum_size : int 


@app.post('/CountNearbyFires')
async def count_nearby_fires(req: FiresCounterBodyRequest):
    
    output = fires_per_city_counter.count_fires_per_city(req.fire_cluster_distance_km, req.city_distance_km, req.fire_group_minimum_size)
    
    return {
    'output': output
    }

# Initiate DB's and Service manager
# Loads raw fires data and a geo json containing city outlines
# TODO - create databases for this raw data and connect to it here
fires_data = FireDataStorage(os.path.join('RawData', 'fire_nrt_V1_189122.csv'))
cities_data = CitiesDataStorage(os.path.join('RawData', 'cities.geojson'))
fires_per_city_counter = FiresPerCityCounterManager(fires_data, cities_data)

if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=8000)

