import numpy as np
import pandas as pd
from data_models import FireDataStorage, CitiesDataStorage
from basic_script import FirePerCityCounter

class FiresPerCityCounterManager:
    def __init__(self, fires_db, cities_db):
        self.fires_db = fires_db
        self.cities_db = cities_db

    def count_fires_per_city(self, fire_cluster_distance_km, city_distance_km, fire_group_minimum_size):
        
        num_of_fires = self.fires_db.size()
        # TODO - divide fires data to chunks and create num of counters for analyse those chunks
        # later we should also merge all the answers to a single output and return it
        counter = FirePerCityCounter(self.fires_db.get_fires_data(0, 10000), self.cities_db)
        result = counter.count_nearby_fires(fire_cluster_distance_km, city_distance_km, fire_group_minimum_size)
        return result
