# README #

### What is this repository for? ###
* Quick summary - This repo contains the code & data for fires_near_cities_counter service
* Version 1.0

### How do I get set up? ###
* Install all the modules appear in requirements.txt 
* Make sure that the following files are in the RawData folder: 'fire_nrt_V1_189122.csv', 'cities.geojson'
* Run main.py 
* A web service will be initialized, documentation and testing will be available at 'http://127.0.0.1:8000/docs#'

### Who do I talk to? ###

* Repo owner - Aviv Nevo
