import geopandas as gp
import pandas as pd, numpy as np, matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from geopy.distance import great_circle
from shapely.geometry import MultiPoint

# TODO - make these classes as a databases with api

class FireDataStorage:
    def __init__(self, file_name):
        print("FireDataStorage Init")
        self.file_name = file_name
        self.fires_data = pd.read_csv(file_name)
        self.fires_data['acq_date'] = pd.to_datetime(self.fires_data['acq_date']) 
    
    def get_fires_data(self, from_row, to_row):
        return self.fires_data.loc[from_row:to_row, :]

    def size(self):
        return self.fires_data.size

class CitiesDataStorage:
    def __init__(self, file_name):
        print("CitiesDataStorage Init")
        self.file_name = file_name
        self.cities_data = gp.GeoDataFrame.from_file(file_name) 
        self.cities_data['centroid'] = self.cities_data.centroid

    def get_cities_data(self):
        return self.cities_data
